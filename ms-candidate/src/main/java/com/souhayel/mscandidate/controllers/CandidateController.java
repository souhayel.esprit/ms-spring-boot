package com.souhayel.mscandidate.controllers;

import com.souhayel.mscandidate.entities.Candidate;
import com.souhayel.mscandidate.entities.Job;
import com.souhayel.mscandidate.entities.User;
import com.souhayel.mscandidate.services.ICandidateService;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/candidates")
public class CandidateController {
    @Autowired
    ICandidateService canService;

    @Autowired
    WebClient.Builder webClientBuilder;

    @PostMapping()
    public Candidate addCandidate(@RequestBody Candidate c){
        System.out.println(c);
        return canService.addCandidate(c);
    }

    @GetMapping()
    public List<Candidate> getAll(){
        return canService.getCandidates();
    }

    @GetMapping("/jobs")
    public List<Job> getJobs(){
        List<Job> jobs =  webClientBuilder.build()
        .get()
        .uri("http://JOB-SERVICE/api/v1/jobs")
        .retrieve()
        .bodyToFlux(Job.class).collectList().block();
        return jobs;
    }

    @GetMapping("/users")
    public List<User> getUsers(){
        List<User> users =  webClientBuilder.build()
        .get()
        .uri("http://USER-SERVICE/api/v1/users")
        .retrieve()
        .bodyToFlux(User.class).collectList().block();
        return users;
    }

    @GetMapping("/{id}")
    public Candidate getOne(@PathVariable("id") Long id){
        return canService.getById(id);
    }
}
