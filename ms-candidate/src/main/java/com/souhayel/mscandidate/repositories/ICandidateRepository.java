package com.souhayel.mscandidate.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.souhayel.mscandidate.entities.Candidate;
import org.springframework.stereotype.Repository;

@Repository
public interface ICandidateRepository extends JpaRepository<Candidate, Long> {
}
