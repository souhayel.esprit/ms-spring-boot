package com.souhayel.mscandidate;

import com.souhayel.mscandidate.entities.Candidate;
import com.souhayel.mscandidate.repositories.ICandidateRepository;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;
import org.springframework.web.reactive.function.client.WebClient;

import java.util.Date;
import java.util.stream.Stream;

@SpringBootApplication
@EnableEurekaClient
public class CandidateApplication {

	public static void main(String[] args) {
		SpringApplication.run(CandidateApplication.class, args);
	}

	@Bean
	ApplicationRunner start(ICandidateRepository repo){
		return args -> {
			Stream.of(Candidate.builder().nom("machfar").prenom("hedi").email("hedi@gmail.com").dateNaissance(new Date()).password("mach123").build(),
					Candidate.builder().nom("machfar").prenom("dali").email("dali@gmail.com").dateNaissance(new Date()).password("mach123").build(),
					Candidate.builder().nom("machfar").prenom("souhayel").email("sou@gmail.com").dateNaissance(new Date()).password("mach123").build()
					).forEach(c ->{
						repo.save(c);
			});
			repo.findAll().forEach(System.out::println);
		};
	}

	@Bean
	@LoadBalanced
	public WebClient.Builder webClientBuilder(){
		return WebClient.builder();
	}
}
