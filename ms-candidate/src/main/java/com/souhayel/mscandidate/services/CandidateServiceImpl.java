package com.souhayel.mscandidate.services;

import com.souhayel.mscandidate.entities.Candidate;
import com.souhayel.mscandidate.repositories.ICandidateRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class CandidateServiceImpl implements ICandidateService{
    @Autowired
    ICandidateRepository candRepo;

    @Override
    public Candidate addCandidate(Candidate c) {
        return candRepo.save(c);
    }

    @Override
    public List<Candidate> getCandidates() {
        return (List<Candidate>) candRepo.findAll();
    }

    @Override
    public Candidate getById(Long id) {
        return candRepo.findById(id).orElse(null);
    }
}
