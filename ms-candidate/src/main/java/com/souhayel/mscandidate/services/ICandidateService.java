package com.souhayel.mscandidate.services;

import com.souhayel.mscandidate.entities.Candidate;

import java.util.List;

public interface ICandidateService {
    public Candidate addCandidate(Candidate c);
    public List<Candidate> getCandidates();
    public Candidate getById(Long id);
}
