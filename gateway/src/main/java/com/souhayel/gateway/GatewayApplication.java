package com.souhayel.gateway;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
@EnableEurekaClient
public class GatewayApplication {

	public static void main(String[] args) {
		SpringApplication.run(GatewayApplication.class, args);
	}

	@Bean
	public RouteLocator gatewayRoutes(RouteLocatorBuilder builder) {
		return builder.routes()
				.route(r -> r.path("/api/v1/candidates/**")
						.uri("lb://CANDIDATE-SERVICE"))

				.route(r -> r.path("/api/v1/jobs/**")
						.uri("lb://JOB_SERVICE"))

				.route(r -> r.path("/api/v1/users/**")
						.uri("lb://USER_SERVICE"))
						
				.build();
	}
}
