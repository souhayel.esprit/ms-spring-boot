const router = require("express").Router();

router.get("/", (req, res) => {
  res.json([
    {
      name: "machfar",
      age: 28,
    },
    {
      name: "souhayel",
      age: 28,
    },
    {
      name: "mohamed",
      age: 28,
    },
  ]);
});

module.exports = router;
