require("dotenv").config();
const express = require("express");
const axios = require("axios");
const eurekaHelper = require("./eureka.helper");
// Routes
const userRouter = require("./routes/userRouter.js");

// Initialize express
const app = express();

const PORT = process.env.PORT || 3000;
const API_PREFIX = process.env.API_PREFIX || 'api/v1';

// Registing to Eureka
eurekaHelper.registerWithEureka("user-service", PORT);

// Mounting routes
app.get(`/${API_PREFIX}/healthcheck`, (req, res) => {
  res.status(200).send("I am node-service");
});
app.use(`/${API_PREFIX}/users`, userRouter);

app.get(`/${API_PREFIX}/jobs`, async (req, res) => {
  const url = `${process.env.JOBS_ENDPOINT}/jobs`;
  const response = await axios.get(url);
  const jobs = response?.data;
  console.log("success | list jobs send to client");
  res.status(200).json({
    status: "success",
    data: jobs,
  });
});

app.get(`/${API_PREFIX}/candidates`, async (req, res) => {
  const url = `${process.env.CANDIDATES_ENDPOINT}/candidates`;
  const response = await axios.get(url);
  const candidate = response?.data;
  console.log("success | list candidate send to client");
  res.status(200).json({
    status: "success",
    data: candidate,
  });
});

app.listen(PORT, () => {
  console.log(`node-service is running on port: ${PORT}`);
});
