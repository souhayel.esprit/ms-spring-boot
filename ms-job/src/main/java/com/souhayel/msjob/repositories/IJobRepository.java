package com.souhayel.msjob.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import com.souhayel.msjob.entities.Job;
import org.springframework.stereotype.Repository;

@Repository
public interface IJobRepository extends JpaRepository<Job, Long> {
}
