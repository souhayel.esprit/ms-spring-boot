package com.souhayel.msjob.services;

import com.souhayel.msjob.entities.Job;

import java.util.List;

public interface IJobService {
    public Job addJob(Job c);
    public List<Job> getJobs();
    public Job getById(Long id);
    public Job updateEtatJobById(Long id);
    public Job updateEtatJobByName(String title);
}
