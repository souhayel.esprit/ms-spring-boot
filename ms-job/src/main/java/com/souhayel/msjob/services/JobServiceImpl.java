package com.souhayel.msjob.services;

import com.souhayel.msjob.entities.Job;
import com.souhayel.msjob.repositories.IJobRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
@Service
public class JobServiceImpl implements IJobService {
    @Autowired
    IJobRepository jobRepo;


    @Override
    public Job addJob(Job j) {
        return jobRepo.save(j);
    }

    @Override
    public List<Job> getJobs() {
        return (List<Job>) jobRepo.findAll();
    }

    @Override
    public Job getById(Long id) {
        return jobRepo.findById(id).orElse(null);
    }

    @Override
    public Job updateEtatJobById(Long id) {
        Job j = jobRepo.findById(id).orElse(null);
        if (j.getEtat() == true){
            j.setEtat(false);
        }else {
            j.setEtat(true);
        }
        jobRepo.save(j);
        return j;
    }

    @Override
    public Job updateEtatJobByName(String title) {
        return null;
    }
}
