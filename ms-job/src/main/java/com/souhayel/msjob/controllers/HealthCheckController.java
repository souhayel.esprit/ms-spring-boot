package com.souhayel.msjob.controllers;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/api/v1/status")
public class HealthCheckController {
    @GetMapping
    public ResponseEntity<HttpStatus> status(){
        return new ResponseEntity<>(HttpStatus.OK);
    }
}
