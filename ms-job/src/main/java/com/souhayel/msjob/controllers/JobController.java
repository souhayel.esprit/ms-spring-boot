package com.souhayel.msjob.controllers;

import com.souhayel.msjob.entities.Job;
import com.souhayel.msjob.services.IJobService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping(value = "/api/v1/jobs")
public class JobController {
    @Autowired
    IJobService jobService;

    @PostMapping()
    public Job addCandidate(@RequestBody Job j){
        return jobService.addJob(j);
    }

    @GetMapping()
    public List<Job> getAll(){
        return jobService.getJobs();
    }

    @GetMapping("/{id}")
    public Job getOne(@PathVariable("id") Long id){
        return jobService.getById(id);
    }

    @PutMapping()
    public Job updateEtat(@RequestParam("id") Long id){
        return jobService.updateEtatJobById(id);
    }

}
