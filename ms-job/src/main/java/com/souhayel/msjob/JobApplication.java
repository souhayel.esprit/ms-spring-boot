package com.souhayel.msjob;

import com.souhayel.msjob.entities.Job;
import com.souhayel.msjob.repositories.IJobRepository;
import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.Bean;

import java.util.stream.Stream;

@SpringBootApplication
@EnableEurekaClient
public class JobApplication {

	public static void main(String[] args) {
		SpringApplication.run(JobApplication.class, args);
	}

	@Bean
	ApplicationRunner start(IJobRepository repo){
		return args -> {
			Stream.of(Job.builder().service("Finance").etat(true).build(),
					Job.builder().service("Dev").etat(false).build()
					).forEach(c ->{
						repo.save(c);
			});
			repo.findAll().forEach(System.out::println);
		};
	}
}
