import { useNavigate } from "react-router-dom";

import "./loginForm.css";
import Input from './../ui/Input';
import Button from './../ui/Button';

function index() {
  const navigate = useNavigate();
  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    console.log(event);
    navigate("/");
  }
  return (
    <form onSubmit={handleSubmit}>
      <Input label="login" type="text"/>
      <Input label="password" type="password" hideEffect={true}/>
      <Button type="submit" value="confirm" />
    </form>
  );
}

export default index;
