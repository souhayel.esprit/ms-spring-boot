import { useNavigate } from "react-router-dom";
import Input from './../ui/Input';
import Button from './../ui/Button';

function index() {
  const navigate = useNavigate();
  const handleSubmit = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    console.log(event)
    navigate("/login")
  }
  return (
    <form onSubmit={handleSubmit}>
      <Input label="name" type="text"/>
      <Input label="login" type="text"/>
      <Input label="password" type="password" hideEffect={true}/>
      <Input label="password confirm" type="password" hideEffect={true}/>
      <Button type="submit" value="register" />
    </form>
  );
}

export default index;
