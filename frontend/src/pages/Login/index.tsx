import "./login.css";
import LoginForm from "./../../components/LoginForm";
import LoginNav from "./../../components/LoginNav";

const index: React.FC = () => {
  return (
    <main className="login-page">
      <section>
        <header>
          <LoginNav />
        </header>
        <section className="login-form-wrapper">
          <h1>Welcome back !</h1>
          <LoginForm />
        </section>
      </section>
      <figure>
        <img src="/images/bg.jpg" alt="mountain bg" />
      </figure>
    </main>
  );
};

export default index;
